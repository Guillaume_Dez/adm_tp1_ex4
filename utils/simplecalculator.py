#!/usr/bin/env python
# coding: utf-8
'''
Création de la classe SimpleCalculator
'''
class SimpleCalculator:
    '''
        This is a class for mathematical operations.

        Attributes:
            varible_un (int): La première variable.
            variable_deux (int): La deuxième variable.
    '''
    result = 0
    def ajout(self, variable_un, variable_deux):
        '''return l'ajout'''
        self.result = variable_un + variable_deux
        print(self.result)

    def soust(self, variable_un, variable_deux):
        '''return la soustraction'''
        self.result = variable_un - variable_deux
        print(self.result)

    def mul(self, variable_un, variable_deux):
        '''return la multiplication'''
        self.result = variable_un * variable_deux
        print(self.result)

    def div(self, variable_un, variable_deux):
        '''return la division'''
        self.result = variable_un / variable_deux
        print(self.result)


###TEST

if __name__ == "__main__":
    '''
        Simple test in case of main call
    '''

VAR_1 = 2
VAR_2 = 10

S = SimpleCalculator()
print("test :")
print(S.ajout(VAR_1, VAR_2))
print(S.soust(VAR_1, VAR_2))
